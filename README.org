#+TITLE: Francisco Javier Velázquez-García's CV
#+AUTHOR: Francisco Javier Velázquez-García
#+EMAIL: francisv@ifi.uio.no
#+OPTIONS: toc:nil

This repository contains the files of Francisco Javier
Velazquez-Garcia's CV.  This is a customized version of this
[[https://www.overleaf.com/latex/templates/a-customised-curve-cv/mvmbhkwsnmwv#.W0tLwRRCQUE][template by Lim Lian Tze]], which is based on the \LaTeX package
[[https://ctan.org/pkg/curve][CurVe by Didier Verna]].  The PDF is built with Emacs and the
AUC-\TeX package, therefore the local variables at the end of
each file.

* What is this repository for?
To share my CV and customization of the templates by [[https://www.overleaf.com/latex/templates/a-customised-curve-cv/mvmbhkwsnmwv#.W0tLwRRCQUE][Lim Lian Tze]] and
[[https://ctan.org/pkg/curve][Didier Verna]].  I welcome feedback.
* Contribution Guidelines
1. Create a new issue in the issue tracker system
2. Send pull requests or patches in email format
* Software Used
- Emacs
- \LaTex
- Biber
* Who Do I Talk To?
- Francisco Javier Velázquez-García
- Email: [[mailto:fj@vg-phd.io][fj at vg-phd.io]]
* Tasks [0/1]
** TODO Add license
